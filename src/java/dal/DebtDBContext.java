/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Debt;

/**
 * Class: DebtDBContext Description: this class have responsibility to manage
 * CRUD of table Debt in database
 *
 * @author Hoang Son
 */
public class DebtDBContext extends DBContext {

    /**
     * method: getAllDebt
     * Description: this method would get all Debt in database which have debtorId
     *              equal with parameter debtorid
     * @param debtorid
     * @return an array list of Debt
     */
    public ArrayList<Debt> getAllDebt(int debtorid) {
        //generate arraylist of debt
        ArrayList<Debt> debtList = new ArrayList<>();

        //String query
        String query = "select * from Debt\n"
                + "where debtorid = ?";

        try {
            PreparedStatement ps = connection.prepareStatement(query);

            //convert debtorid to String
            String debtorID = String.valueOf(debtorid);
            ps.setString(1, debtorID);

            ResultSet rs = ps.executeQuery();

            //Loop: add each debt into array list
            while (rs.next()) {

                //generate a debt object
                Debt debt = new Debt();
                debt.setDebtId(rs.getInt("debtid"));
                debt.setDebtValue(rs.getInt("debtValue"));
                debt.setCreateDate(rs.getDate("createDate"));
                debt.setDescription(rs.getString("description"));
                debt.setType(rs.getBoolean("type"));
                debt.setStatus(rs.getBoolean("status"));
                debt.setDebtorId(rs.getInt("debtorid"));
                debt.setUserId(rs.getInt("userid"));

                //add debt into array list
                debtList.add(debt);
            }

            //return debt list
            return debtList;

        } catch (SQLException ex) {
            Logger.getLogger(DebtDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

        return debtList;
    }

    /**
     * Method: insertDebt
     * Description: this method would insert new Debt into table Debt
     * @param debtValue
     * @param date
     * @param description
     * @param type
     * @param debtorId
     * @param userId
     * @return 1 if insert success or 0 if fail
     */
    public int insertDebt(String debtValue, Date date, String description, 
                            String type, int debtorId, int userId) {

        //String query
        String query = "INSERT INTO [Debt]\n"
                + "           ([debtValue]\n"
                + "           ,[createDate]\n"
                + "           ,[description]\n"
                + "           ,[type]\n"
                + "           ,[status]\n"
                + "           ,[debtorid]\n"
                + "           ,[userid])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            PreparedStatement ps = connection.prepareStatement(query);
            
            //change date to String
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String D = df.format(date);
            
            //set
            ps.setString(1, debtValue);
            ps.setString(2, D);
            ps.setString(3, description);
            ps.setString(4, type);
            
            //set default status is false
            ps.setString(5, "false");
            
            ps.setString(6, String.valueOf(debtorId));
            ps.setString(7, String.valueOf(userId));
            
            int check = ps.executeUpdate();
            
            return check;

        } catch (SQLException ex) {
            Logger.getLogger(DebtDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 0;
    }

}
