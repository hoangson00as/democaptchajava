/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Debtor;

/**
 * Class: DebtorDBContext Description: this class have responsibility to manage
 * CRUD of table Debtor in database
 *
 * @author Hoang Son
 */
public class DebtorDBContext extends DBContext {

    /**
     * Method: getDebtorList Description: this method would get all debtor which
     * have userId equal to parameter userId in the table debtor of database
     *
     * @param userid
     * @return DebtorList an array list of Debtor
     */
    public ArrayList<Debtor> getDebtorList(int userid) {
        //generate a new ArrayList of Debtor
        ArrayList<Debtor> DebtorList = new ArrayList<>();

        //String query
        String query = "select * from Debtor \n"
                + "where userid = ?";

        try {
            //prepared statement
            PreparedStatement ps = connection.prepareStatement(query);

            //set userid
            ps.setInt(1, userid);

            ResultSet rs = ps.executeQuery();

            //Loop: take each object debtor and add into arraylist
            while (rs.next()) {
                //generate new debtor object
                Debtor debtor = new Debtor();
                debtor.setDebtorId(rs.getInt("debtorid"));
                debtor.setDebtorName(rs.getString("debtorName"));
                debtor.setDebtorAddress(rs.getString("debtorAddress"));
                debtor.setPhoneNumber(rs.getString("phoneNumber"));
                debtor.setDebtorEmail(rs.getString("debtorEmail"));
                debtor.setCreateDate(rs.getDate("createDate"));
                debtor.setUpdateDate(rs.getDate("updateDate"));
                debtor.setUserId(rs.getInt("userid"));

                //add debtor into arraylist
                DebtorList.add(debtor);
            }

            //return debtor list
            return DebtorList;

        } catch (SQLException ex) {
            Logger.getLogger(DebtorDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

        return DebtorList;
    }

    /**
     * Method: addDebtor Description: this method would add a new debtor object
     * into table Debtor of the database with parameter: debtorName,
     * debtorAddress, phoneNumber, debtorEmail, createDate, updateDate, userId
     *
     * @param debtorName
     * @param debtorAddress
     * @param phoneNumber
     * @param debtorEmail
     * @param createDate
     * @param updateDate
     * @param userId
     * @return 1 if add access and 0 if fail
     */
    public int addDebtor(String debtorName, String debtorAddress, String phoneNumber,
            String debtorEmail, Date createDate, Date updateDate, int userId) {

        //generate a String query
        String query = "INSERT INTO [dbo].[Debtor]\n"
                + "           ([debtorName]\n"
                + "           ,[debtorAddress]\n"
                + "           ,[phoneNumber]\n"
                + "           ,[debtorEmail]\n"
                + "           ,[createDate]\n"
                + "           ,[updateDate]\n"
                + "           ,[userid])\n"
                + "     VALUES\n"
                + "           (?"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        

        try {
            PreparedStatement ps = connection.prepareStatement(query);

            //set
            ps.setString(1, debtorName);
            ps.setString(2, debtorAddress);
            ps.setString(3, phoneNumber);
            ps.setString(4, debtorEmail);
            
            //change Date type to String
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            
            String cDate = df.format(createDate);
            String uDate = df.format(updateDate);
            
            ps.setString(5, cDate);
            ps.setString(6, uDate);
            ps.setInt(7, userId);
            
            int check = ps.executeUpdate();

            return check;

        } catch (SQLException ex) {
            Logger.getLogger(DebtorDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 0;
    }

}
