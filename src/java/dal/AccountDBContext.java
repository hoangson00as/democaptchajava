/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;

/**
 * Class: AccountDBContext
 * Description: This class have responsibility to connect to table Account in 
 *              database
 * @author Hoang Son
 */
public class AccountDBContext extends DBContext {
    
    /**
     * Method: getAccount
     * Description: this method would get username, email from table Account in 
     *              database by username and password
     * @param username
     * @param password
     * @return Account a account with username and password equal to parameter
     *                  or null if there is no account have that username and 
     *                  password
     */
    public Account getAccount(String username, String password) {
//        Account account = new Account();

        String query = "select username, email, roleID from Account\n"
                + "where username = ? and password = ?";

        try {
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, username);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                Account acc = new Account();
                acc.setUsername(rs.getString("username"));
                acc.setEmail(rs.getString("email"));
                acc.setRoleID(rs.getInt("roleID"));

                return acc;
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    /**
     * Method: getAllAccount
     * Description: this method will get all account in table Accoutn of database
     *              then add into an array list
     * @return listAccount an array list of Account
     */
    //get all account from database
    public ArrayList<Account> getAllAccount() {

        //generate an arrayList to store account
        ArrayList<Account> listAccount = new ArrayList<>();
        String query1 = "select * from Account";

        try {
            PreparedStatement ps = connection.prepareStatement(query1);

            ResultSet rs = ps.executeQuery();

            //Loop: read each row in database and add into arrayList
            while (rs.next()) {
                Account acc = new Account();
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setEmail(rs.getString("email"));
                acc.setRegisterDate(rs.getDate("registerDate"));
                acc.setStatus(rs.getBoolean("status"));
                acc.setRoleID(rs.getInt("roleID"));

                //add object account to arrayList
                listAccount.add(acc);
            }

            return listAccount;

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listAccount;
    }
    
    /**
     * Method: getUser
     * Description: This method would get an account which have username 
     *              equal to parameter from table Account of database 
     * @param username
     * @return acc an Account if it have username equal to parameter username
     *              or null other wise
     */
    //check username existed or not
    public Account getUser(String username) {
        //String query
        String query = "select * from Account where username = ?";

        try {
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                Account acc = new Account();
                acc.setUsername(rs.getString("username"));
                acc.setEmail(rs.getString("email"));

                return acc;
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * Method: addAccount
     * Description: this method would add an Account into table Account of 
     *              database 
     * @param username
     * @param password
     * @param email
     * @param date
     * @return 1 if success add into database
     *          0 if fail to add into database
     */
    public int addAccount(String username, String password, String email, Date date) {
        //String query: add new object Account into database
        String query = "INSERT INTO [Account]\n"
                + "           ([username]\n"
                + "           ,[password]\n"
                + "           ,[email]\n"
                + "           ,[registerDate]\n"
                + "           ,[status])\n"
                + "     VALUES\n"
                + "           (?"
                + "           ,?"
                + "           ,?"
                + "           ,?"
                + "           ,?)"
                + "INSERT INTO [User]\n"
                + "           ([username])\n"
                + "            VALUES\n"
                + "           (?)";
                

        try {
            PreparedStatement ps = connection.prepareStatement(query);

            //convert date to string
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String dateRegister = df.format(date);

            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, email);
            ps.setString(4, dateRegister);
            ps.setString(5, "false");
            ps.setString(6, username);
            
            int rs = ps.executeUpdate();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     * Method: deleteAccount
     * Description: this method would delete an account in database where 
     *              username equal to parameter username
     * @param username
     * @return 1 if success delete and 0 if fail to delete
     */
//    public int deleteAccount(String username) {
//
//        //String query
//        String query = "DELETE FROM [dbo].[Account]\n"
//                + "      WHERE username = ? "
//                + "      DELETE CASCADE";
//
//        try {
//            PreparedStatement pst = connection.prepareStatement(query);
//            pst.setString(1, username);
//
//            //checkDel = 1 if delete success
//            int checkDel = pst.executeUpdate();
//
//            return checkDel;
//        } catch (SQLException ex) {
//            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return 0;
//    }

}
