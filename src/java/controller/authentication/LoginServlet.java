/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.authentication;

import dal.AccountDBContext;
import dal.UserDBContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import model.Account;
import model.User;

/**
 *
 * @author dell
 */
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //take raw info
        String rawUser = req.getParameter("rawUser");
        String rawPass = req.getParameter("rawPass");
        
        resp.getWriter().println(rawUser);
        resp.getWriter().println(rawPass);
        
        //get account
        AccountDBContext AccCon = new AccountDBContext();
        Account acc = AccCon.getAccount(rawUser, rawPass);
        
        resp.getWriter().println("----------");
        resp.getWriter().println("username: "+acc.getUsername());
        
        if(acc == (null)){
            resp.getWriter().println("login fail!");
        }else{
            req.getSession().setAttribute("account", acc);
            
            //get user
            UserDBContext userContext = new UserDBContext();
            
            User user = userContext.getUser(acc.getUsername());
            
            //set sesssion user
            req.getSession().setAttribute("user", user);
            
            resp.getWriter().println("login successful!");
            
            resp.sendRedirect("../home");
        }
        
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("../view/user/login.jsp").forward(req, resp);
    }
    
}
