/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.register;

import dal.AccountDBContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import model.Account;

/**
 *
 * @author Hoang Son
 */
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //take raw info
        String rawUser = req.getParameter("rawUser");
        String rawPass = req.getParameter("rawPass");
        String rawConfirm = req.getParameter("rawConfirm");
        String rawEmail = req.getParameter("rawEmail");
        String captcha = req.getParameter("captcha");

        //validation
        boolean valid = true;

        //generate string to show for user
        String error = "";

        //valid username: character from 6 to 12 chars
        if (!rawUser.matches("^[a-zA-Z0-9]{6,12}$")) {
            error += " Username is not valid!";
            valid = false;
        }

        //valid password: 6 - 18 char, digit
        if (!rawPass.matches("^[0-9a-zA-Z]{6,18}$")) {
            error += " Password is not valid!";
            valid = false;
        }

        //validate confirm password
        if (rawPass.matches("^[0-9a-zA-Z]{6,18}$")) {

            //check confirm password equal password or not
            if (!rawPass.equals(rawConfirm)) {
                error += " Confirm password is not equals with password!";
                valid = false;

            }
        } else {
            error += " Confirm password is not valid!";
            valid = false;
        }

        //validate email: "^\S+@\S+\.\S+$"
        if (!rawEmail.matches("^\\S+@\\S+\\.\\S+$")) {
            valid = false;
            error += " Email is not valid!";
        }

        /*captcha*/
        // Get the captcha value stored in the session
        HttpSession session = req.getSession();
        String storedCaptcha = (String) session.getAttribute("captcha");

        // Validate the user input
        if (captcha.equals(storedCaptcha)) {
            // Captcha is correct, save the user information to a database
            
            // notification
            resp.getWriter().println("right captcha");

        } else {
            // Captcha is incorrect, show an error message

            // notification
            resp.getWriter().println("wrong captcha");
            error += " Captcha is not correct!";
            valid = false;
        }
        

        //if not valid -> show error
        if (!valid) {
            //set error session
            req.getSession().setAttribute("error", error);

            //back to register and show error
            resp.sendRedirect("register");
        } //else: username, password is valid -> add into database
        else {
            //db contact
            AccountDBContext AccCon = new AccountDBContext();

            //check username existed or not
            Account acc = AccCon.getUser(rawUser);

            //if acc == null -> not existed before -> created new
            if (acc == null) {

                //register account: add new user into database with status is: false -> not verify
                //take current date -> add into database as: dateRegister
                Date date = new Date();

                //add new account into database
                int add = AccCon.addAccount(rawUser, rawPass, rawEmail, date);

                //if add success 
                if (add == 1) {
                    //show notification: register success
                    resp.getWriter().println("register success");

                } else {
                    resp.getWriter().println("register fail");
                }

                //send verify email
                
                //back to login page
                resp.sendRedirect("../user/login");
            }
            //else -> show alert to tell user choose other username
            else {
                //error: account have existed
                String errorUsernameExisted = "username have already existed! Please input other username!";
                req.getSession().setAttribute("error", errorUsernameExisted);

                //back to register with error alert                
                resp.sendRedirect("register");
            }

        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("../view/user/register.jsp").forward(req, resp);
    }

}
