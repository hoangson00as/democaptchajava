/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;


//@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // Get the values entered by the user
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    String captcha = request.getParameter("captcha");

    // Get the captcha value stored in the session
    HttpSession session = request.getSession();
    String storedCaptcha = (String) session.getAttribute("captcha");

    // Validate the user input
    if (captcha.equals(storedCaptcha)) {
      // Captcha is correct, save the user information to a database

      // notification
      response.getWriter().println("right captcha");
      
    } else {
      // Captcha is incorrect, show an error message

      // notification
      response.getWriter().println("wrong captcha");
    }
  }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("register.jsp").forward(req, resp);
    }
  
  
}

