/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.debtor;

import controller.authentication.BaseAuthenticationController;
import dal.DebtorDBContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import model.Account;
import model.Debtor;
import model.User;

/**
 *
 * @author dell
 */
public class DebtorServlet extends BaseAuthenticationController {

    @Override
    protected void processPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        
        
    }

    @Override
    protected void processGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get all debtor list in database
        
        //DebtorDBContext
        DebtorDBContext debtorContext = new DebtorDBContext();
        
        //get userid
        User user = (User) req.getSession().getAttribute("user");
        
        //get debtor list
        ArrayList<Debtor> DebtorList = debtorContext.getDebtorList(user.getUserId());
        
        //add debtorlist into session
        req.getSession().setAttribute("debtorList", DebtorList);
        
        //test
        resp.getWriter().println("test: "+DebtorList);
        
        //move to view/debtor/debtorlist.jsp
        req.getRequestDispatcher("view/debtor/debtorlist.jsp").forward(req, resp);
    }
    
}
