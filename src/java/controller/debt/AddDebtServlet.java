/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.debt;

import controller.authentication.BaseAuthenticationController;
import dal.DebtDBContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import model.Debtor;
import model.User;

/**
 *
 * @author dell
 */
public class AddDebtServlet extends BaseAuthenticationController {

    @Override
    protected void processPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        //take info 
        String debtValue = req.getParameter("debtValue");
        String description = req.getParameter("description");
        String type = req.getParameter("type");
        
        //take debtorId
        int debtorId = (int) req.getSession().getAttribute("debtorId");
        
        //take userId
        User user = (User) req.getSession().getAttribute("user");
        int userId = user.getUserId();
        
        //take current date
        Date date = new Date();
        
        //test
        resp.getWriter().println("debtValue "+debtValue);
        resp.getWriter().println("description "+description);
        resp.getWriter().println("type "+type);
        resp.getWriter().println("debtorId "+debtorId);
        resp.getWriter().println("userId "+userId);
        resp.getWriter().println("date "+date);
        
        //Validation
        
        //generate a boolean value to check validation
        boolean valid = true;

        //generate string to show for user
        String error = "";

        //valid debtValue: digit
        if (!debtValue.matches("^[0-9]{1,}$")) {
            error += " Debt is not valid!";
            valid = false;
        }
        
        //if valid != true
        if(!valid){
            
            //set session error
            req.getSession().setAttribute("errorAddDebt", error);
            
            //back to adddebt.jsp
            resp.sendRedirect("insertdebt");
        }
        //else: add into database
        else{
            
            //generate a new DebtDBContext
            DebtDBContext debtContext = new DebtDBContext();
            
            //add into database
            int check = debtContext.insertDebt(debtValue, date, description, type, debtorId, userId);
           
            
            //if check = 1 -> insert success
            if(check == 1){
                resp.getWriter().println("insert success!");
                
                //back to debtlist
                resp.sendRedirect("debt?debtorId="+debtorId);
            }else if(check == 0){
                resp.getWriter().println("insert fail!");
                
                req.getSession().setAttribute("errorAddDebt", " Insert fail!");
                
                //back to insertdebt
                resp.sendRedirect("insertdebt");
            }
        }
        
        
    }

    @Override
    protected void processGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        req.getRequestDispatcher("view/debt/adddebt.jsp").forward(req, resp);
        
    }
    
}
