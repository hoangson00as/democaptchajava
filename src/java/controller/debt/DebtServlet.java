/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.debt;

import controller.authentication.BaseAuthenticationController;
import dal.DebtDBContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import model.Debt;

/**
 *
 * @author dell
 */
public class DebtServlet extends BaseAuthenticationController {

    @Override
    protected void processPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
    }

    @Override
    protected void processGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        //set session debtorId to null
        req.getSession().setAttribute("debtorId", null);
        
        //take debtorid
        int debtorId = Integer.parseInt(req.getParameter("debtorId"));
        
        //set debtorId to session
        req.getSession().setAttribute("debtorId", debtorId);
        
        //get all debt by debtorid
        DebtDBContext debtContext = new DebtDBContext();
        ArrayList<Debt> debtList = debtContext.getAllDebt(debtorId);
        
        //set session debtList
        req.getSession().setAttribute("debtList", debtList);
        
        //move to debtlist.jsp
        req.getRequestDispatcher("view/debt/debtlist.jsp").forward(req, resp);
    }
    
}
