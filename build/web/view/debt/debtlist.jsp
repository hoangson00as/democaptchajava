<%-- 
    Document   : debtlist
    Created on : Feb 13, 2023, 3:44:09 PM
    Author     : dell
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <title>JSP Page</title>

    </head>
    <style>

        .Detail{
            background-color: #63c2de;
        }
        .click{
            margin: 10px;
            background-color:  #63c2de;
            border-radius: 0.25rem;
        }
        .-pagination{
            display: flex;
            flex-wrap: wrap;
            padding: 3px;
            -webkit-box-shadow: 0 0 15px 0 rgb(0 0 0 / 10%);
            box-shadow: 0 0 15px 0 rgb(0 0 0 / 10%);
            border-top: 2px solid rgba(0,0,0,0.1)
        }
        .-center{
            display: flex
        }
        .-pageInfo{
            display: flex
        }

        .-previous{
            flex: 1 1;
            text-align: center;
        }
        .-center{
            flex: 1.5 1;
            text-align: center;
            margin-bottom: 0;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -ms-flex-align: center;
            align-items: center;
            -ms-flex-pack: distribute;
            justify-content: space-around;
        }
        .-next{
            flex: 1 1;
            text-align: center;
        }
        .-next button{
            appearance: none;
            display: block;
            width: 100%;
            height: 100%;
            border: 0;
            border-radius: 3px;
            padding: 6px;
            font-size: 1em;
            color: rgba(0,0,0,0.6);
            background: rgba(0,0,0,0.1);
            -webkit-transition: all .1s ease;
            -o-transition: all .1s ease;
            transition: all .1s ease;
            cursor: pointer;
            outline: none;
        }
        .-previous button{
            appearance: none;
            display: block;
            width: 100%;
            height: 100%;
            border: 0;
            border-radius: 3px;
            padding: 6px;
            font-size: 1em;
            color: rgba(0,0,0,0.6);
            background: rgba(0,0,0,0.1);
            -webkit-transition: all .1s ease;
            -o-transition: all .1s ease;
            transition: all .1s ease;
            cursor: pointer;
            outline: none;
        }
        .d-grid{
            display: grid;
        }
    </style>
    <body>
        <h1>Danh sách khoản nợ</h1>

        <button class="click">  <a href="insertdebt">Thêm khoản nợ</a></button>

        <table border="1"class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Ghi chú</th>
                    <th>Loại nợ</td>
                    <th>Số tiền</th>
                    <th>Thời gian tạo</th>
                    <th>Action</th>

                </tr>
            </thead>
            <tr>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder="From"
                            />
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="To" />
                    </div>

                </th>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder="From"
                            />
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="To" />
                    </div>

                </th>

                <th>
                    <span class="select-wrap -pageSizeOptions"><select><option value="">all</option><option value="1">Khoản cho vay</option><option value="">Khoản nợ</option></select></span>
                </th>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder="From"
                            />
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="To" />
                    </div>
                </th>



                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder="From"
                            />
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="To" />
                    </div>

                </th>
                <th>
                    <div class="d-grid">
                        <button type="button" class="btn btn-outline-danger mb-3">
                            CLEAR FILTER
                        </button>
                        <button type="button" class="btn btn-outline-info">
                            HIDE >>
                        </button>
                    </div>
                </th>
            </tr>
            <c:forEach items="${sessionScope.debtList}" var="debt">
                <tr>
                    <c:if test="${debt.type eq true}">
                        <td style="color:red">${debt.debtId}</td>
                        <td style="color:red">${debt.description}</td>
                        <td style="color:red">Khoản nợ</td>
                        <td style="color:red">${debt.debtValue}</td>
                        <td style="color:red">${debt.createDate}</td>

                    </c:if>
                    <c:if test="${debt.type eq false}">
                        <td style="color:green">${debt.debtId}</td>
                        <td style="color:green">${debt.description}</td>
                        <td style="color:green">Khoản cho vay</td>
                        <td style="color:green">${debt.debtValue}</td>
                        <td style="color:green">${debt.createDate}</td>
                    </c:if>




                    <td><div><button type="button" data-toggle="tooltip" title="Chi tiết" class="mr-1 btn btn-info"><i class="fa fa-info"></i> Chi tiết</button><br>
                            <button type="button" data-toggle="tooltip" title="  " class="mr-1 btn btn-danger disabled"><i class="fa fa-minus">-</i> 
                            </button>
                            <button type="button" data-toggle="tooltip" title="   " class="mr-1 btn btn-success"><i class="fa fa-plus">+</i>    </button></div></td>


                </tr>
            </c:forEach>
        </table>
        <div class="ui"> <div class="-pagination"><div class="-previous"><button type="button" class="-btn" disabled="">Previous</button></div><div class="-center"><span class="-pageInfo">Page <div class="-pageJump"><input type="number" value="1"></div> /  <span class="-totalPages">2</span></span><span class="select-wrap -pageSizeOptions"><select><option value="5">5 Records</option><option value="10">10 Records</option><option value="20">20 Records</option><option value="25">25 Records</option><option value="50">50 Records</option><option value="100">100 Records</option></select></span></div><div class="-next"><button type="button" class="-btn">Next</button></div></div>
        </div> 
        <button class="click">  <a href="debtor">Back to debtor list</a></button>

    </body>
</html>
