<%-- 
    Document   : home
    Created on : Jan 17, 2023, 2:41:05 PM
    Author     : dell
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link href="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.7/dist/css/coreui.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="./view/CSS/list_Debit.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>


    </head>
    <body>
    <body class="header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden aside-menu-off-canvas a " id="body">
        <div id="root">
            <div class="app">
                <header class="app-header navbar">
                    <button type="button" class="d-lg-none navbar-toggler" data-sidebar-toggler="true">
                        <span class="navbar-toggler-icon">
                        </span><!-- comment -->
                    </button>
                    <a full="[object Object]" minimized="[object Object]" class="navbar-brand">
                        <img src="ASSESTS/IMG/mama.png" width="auto" height="25" alt=" avatar" class="navbar-brand-full">
                        <img src="ASSESTS/IMG/mama.png" width="30" height="30" alt=" avatar" class="navbar-brand-minimized">
                    </a><!-- comment -->

                    <button type="button" class="d-md-down-none navbar-toggler menu" data-sidebar-toggler="true">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <ul class="ml-auto navbar-nav">
                        <li class="dropdown nav-item"></li>
                        <li class="dropdown nav-item">
                            <a aria-haspopup="true" href="#" class="nav-link" aria-expanded="false" data-bs-toggle="dropdown">
                                <p class="mr-3 mt-3">Login as: ${sessionScope.account.username} </p>
                            </a>
                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                <button type="button" tabindex="0" class="dropdown-item">
                                    <a href="#/profile?page=16"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0Zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4Zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10Z"/>
                                        </svg> User infomation</a>
                                </button>
                                <button type="button" tabindex="0" class="dropdown-item" onclick="window.location.href = 'logout'">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lock" viewBox="0 0 16 16">
                                    <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2zM5 8h6a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1z"/>
                                    </svg> Sign-out</button>
                            </div>
                        </li>
                    </ul>

                </header>
                
                    

                <div class="app-body">
                    <div class="sidebar">
                        <div class="sidebar-nav ps scrollbar-container ps--active-y">
                            <ul class="nav"><li class="nav-item"><a class="nav-link" href="dashboard">
                                        <i class="nav-icon icon-home">

                                        </i>Trang chủ
                                    </a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="list_debit">
                                        <i class="nav-icon fa fa-sticky-note-o">

                                        </i>Sổ ghi nợ
                                    </a>
                                </li>
                            </ul>
                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;">

                                </div>

                            </div>
                            <div class="ps__rail-y" style="top: 0px; right: 0px;">
                                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;">

                                </div>

                            </div>

                        </div>
                        <button class="sidebar-minimizer mt-auto menu" type="button">
                        </button>
                    </div>
                    <a href="debtor">Debtor list</a>
                    </body>
                    </html>
