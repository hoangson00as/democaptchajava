<%-- 
    Document   : notification
    Created on : Feb 9, 2023, 3:37:33 PM
    Author     : dell
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Alert:</h1>
        <c:if test="${sessionScope.notification ne null}">
            <h3>${sessionScope.notification}</h3>
        </c:if>
            <a href="user/login">Login</a> <br>
            <a href="user/register">Register</a>
    </body>
</html>
