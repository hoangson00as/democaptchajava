<%-- 
    Document   : login
    Created on : Nov 25, 2022, 10:32:09 PM
    Author     : dell
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body{
                margin: 0;
                padding: 0;
                font-family: 'Courier New', Courier, monospace;
                background: linear-gradient(120deg, #2980b9, #8e44ad);
                height: 100vh;
                overflow: hidden;
            }

            .center{
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                width: 400px;
                border-radius: 10px;
                background: white;

            }

            .center h1{
                text-align: center;
                padding: 0 0 20px 0;
                border-bottom: 1px solid silver;
            }

            .center form{
                padding: 0 40px;
                box-sizing: border-box;
            }

            form .txt_field{
                position: relative;
                border-bottom: 2px solid #adadad;
                margin: 30px 0;

            }

            .txt_field input{
                width: 100%;
                padding: 0 0px;
                height: 40px;
                font-size: 16px;
                border: none;
                background: none;
                outline: none;
            }

            .txt_field label{

                position: absolute;
                top: 50%;
                left: 5px;
                color: #adadad;
                transform: translateY(-50%);
                font-size: 16px;
                pointer-events: none;
                transition: .5s;
            }

            .txt_field span::before{
                content: '';
                position: absolute;
                top: 40px;
                left: 0;
                width: 0%;
                height: 2px;
                background: #2691d9;
                transition: .5s;
            }

            .txt_field input:focus ~ label,
            .txt_field input:valid ~ label{
                top: -5px;
                color: #2691d9;
            }

            .txt_field input:focus ~ span::before,
            .txt_field input:valid ~ span::before{
                width: 100%;
            }

            .pass{
                margin: -5px 0 20px 5px;
                color: #a6a6a6;
                cursor: pointer;
            }

            .pass:hover{
                text-decoration: underline;
            }

            button[type="submit"]{
                width: 100%;
                height: 50px;
                border: 1px solid;
                background: #2691d9;
                border-radius: 25px;
                font-size: 18px;
                color: #e9f4fb;
                font-weight: 700;
                cursor: pointer;
                outline: none;
            }

            button[type="submit"]:hover{
                border-color: #2691d9;
                transition: .5s;
            }

            .signup_link{
                margin: 30px 0;
                text-align: center;
                font-size: 16px;
                color: #666666;
            }

            .signup_link a{
                color: #2691d9;
                text-decoration: none;

            }

            .signup_link a:hover{
                text-decoration: underline;
                transition: .5s;
            }

            .center h4{
                text-align: center;
                padding: 0 0 20px 0;
                color: red;
            }
            .center h5{
                color: red;
                margin: 0;
            }
        </style>
    </head>


    <!--html code-->
    <div class="center">
        <h1>Forget</h1>
        
        <form action="register" method="post">
            
            <div class="txt_field">
                <input type="text" name="rawUser" required>
                <label>Username</label>
            </div>
            <span></span>


            <div class="txt_field">
                <input type="text" name="rawEmail" required>
                <span></span>
                <label>Email</label>
            </div>

            <!-- Captcha -->
            <img src="../captcha" alt="Captcha image">
            
            <div class="txt_field">
                <input type="text" name="captcha" required>
                <span></span>
                <label>Enter captcha</label>
            </div>
            
            <!--jsp code-->            
            <c:if test="${sessionScope.error ne null}">
                <h4>${sessionScope.error}</h4>
            </c:if>

            <!--<div class="pass">Forgot password?</div>-->

            <!--<input type="submit" value="Login">-->
            <button type="submit">Forget PassWord</button>

            <div class="signup_link">
                <!--Not a member? <a href="#">Signup</a>-->
            </div>
        </form>
    </div>




</html>
