<%-- 
    Document   : debtorlist
    Created on : Feb 9, 2023, 7:28:08 PM
    Author     : dell
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    </head>
    <style>
        .Detail{
            background-color: #63c2de;
        }
        .click{
            margin: 10px;
            background-color:  white;
            border-radius: 0.25rem;
        }
        .-pagination{
            display: flex;
            flex-wrap: wrap;
            padding: 3px;
            -webkit-box-shadow: 0 0 15px 0 rgb(0 0 0 / 10%);
            box-shadow: 0 0 15px 0 rgb(0 0 0 / 10%);
            border-top: 2px solid rgba(0,0,0,0.1)
        }
        .-center{
            display: flex
        }
        .-pageInfo{
            display: flex
        }

        .-previous{
            flex: 1 1;
            text-align: center;
        }
        .-center{
            flex: 1.5 1;
            text-align: center;
            margin-bottom: 0;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -ms-flex-align: center;
            align-items: center;
            -ms-flex-pack: distribute;
            justify-content: space-around;
        }
        .-next{
            flex: 1 1;
            text-align: center;
        }
        .-next button{
            appearance: none;
            display: block;
            width: 100%;
            height: 100%;
            border: 0;
            border-radius: 3px;
            padding: 6px;
            font-size: 1em;
            color: rgba(0,0,0,0.6);
            background: rgba(0,0,0,0.1);
            -webkit-transition: all .1s ease;
            -o-transition: all .1s ease;
            transition: all .1s ease;
            cursor: pointer;
            outline: none;
        }
        .-previous button{
            appearance: none;
            display: block;
            width: 100%;
            height: 100%;
            border: 0;
            border-radius: 3px;
            padding: 6px;
            font-size: 1em;
            color: rgba(0,0,0,0.6);
            background: rgba(0,0,0,0.1);
            -webkit-transition: all .1s ease;
            -o-transition: all .1s ease;
            transition: all .1s ease;
            cursor: pointer;
            outline: none;
        }
    </style>

    <body>
        <h1>Danh sách người nợ</h1>
         <p class="mr-3 mt-3">Login as: ${sessionScope.account.username}</p>
<!--         <p class="small text-muted m-0">Total: ${requestScope.debtorList} Record(s)</p>-->
         <button class="click"> <a href="adddebtor" >Thêm người nợ</a></button> 
        <table border="1"class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th>Tên</th>
                    <th>Địa chỉ</td>
                    <th>Số điện thoại</th>
                    <th>Email</th>
                    <th>Tổng nợ</th>  
                    <th>Ngày tạo</th>
                    <th>Ngày cập nhật</th>
                      
                    <th>Action</th>
                </tr>
            </thead>
            <tr>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder="From"
                            />
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="To" />
                    </div>
                </th>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                            />
                    </div>
                </th>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                            />
                    </div>
                </th>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                            />
                    </div>
                </th>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                            />
                    </div>
                </th>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder="From"
                            />
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="To" />
                    </div>
                </th>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder="From"
                            />
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="To" />
                    </div>
                </th>
                <th>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            placeholder="From"
                            />
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="To" />
                    </div>
                </th>
                <th>
                    <div class="d-grid">
                        <button type="button" class="btn btn-outline-danger mb-3">
                            CLEAR FILTER
                        </button>
                        <button type="button" class="btn btn-outline-info">
                            HIDE >>
                        </button>
                    </div>
                </th>
            </tr>
            <c:forEach items="${sessionScope.debtorList}" var="debtor">
                <tr>
                    <td>${debtor.debtorId}</td>
                    <td>${debtor.debtorName}</td>
                    <td>${debtor.debtorAddress}</td>
                    <td>${debtor.phoneNumber}</td>
                    <td>${debtor.debtorEmail}</td>
                    <td>${debtor.userId}</td>
                    <td>${debtor.createDate}</td>
                    <td>${debtor.updateDate}</td>
                    
                    <td>
                        <form action="debt" method="get">
                            
                            <div>
                                <button type="hidden" value="${debtor.debtorId}" name="debtorId" type="submit" title="Chi tiết" class="mr-1 btn btn-info"><i class="fa fa-info"></i> Chi tiết</button>
                                <a href="insertdebt">
                                    <button type="button" data-toggle="tooltip" title="Thêm phiếu nợ" class="mr-1 btn btn-outline-success"><i class="fa fa-plus"></i> Thêm phiếu nợ</button>
                                </a>
                                
                                <button type="button" data-toggle="tooltip" title="Sửa   " class="mr-1 btn btn-warning"><i class="fa fa-pencil"></i> Sửa   </button>
                            </div>
                        </form>                       
                    </td>
                </tr>
            </c:forEach>

        </table>
        <div class="ui"> <div class="-pagination"><div class="-previous"><button type="button" class="-btn" disabled="">Previous</button></div><div class="-center"><span class="-pageInfo">Page 
                        <div class="-pageJump"><input type="number" value="1"></div> /  <span class="-totalPages">2</span></span>
                        <span class="select-wrap -pageSizeOptions"><select><option value="5">5 Records</option><option value="10">10 Records</option><option value="20">20 Records</option><option value="25">25 Records</option><option value="50">50 Records</option><option value="100">100 Records</option></select></span></div>
                        <div class="-next"><button type="button" class="-btn">Next</button></div></div>
        </div> 

    </body>
</html>
